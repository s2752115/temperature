package main.java.nl.utwente.di.temperature;

public class Quoter {

    public double getFahrenheit(String isbn) {
        double celsius = Double.parseDouble(isbn);
        return celsius * 1.8 + 32;
    }

}
