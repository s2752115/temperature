package main.java.nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestQuoter {

    @Test
    public void testFahrenheit() throws Exception {
        Quoter quoter = new Quoter();
        double celsius = quoter.getFahrenheit("100");
        Assertions.assertEquals(212.0, celsius, 0.0);
    }

}
